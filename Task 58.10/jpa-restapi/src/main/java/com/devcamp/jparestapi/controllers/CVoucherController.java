package com.devcamp.jparestapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jparestapi.models.CVoucher;
import com.devcamp.jparestapi.responsitory.IVoucherRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherController {
    
    @Autowired
    IVoucherRepository iVoucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVoucher(){
        try {
            List<CVoucher> list = new ArrayList<>();
            iVoucherRepository.findAll().forEach(list::add);

            return new ResponseEntity<List<CVoucher>>(list, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

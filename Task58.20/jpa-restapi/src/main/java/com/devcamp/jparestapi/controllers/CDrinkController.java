package com.devcamp.jparestapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jparestapi.models.CDrink;
import com.devcamp.jparestapi.responsitory.IDrinkResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController  {
    @Autowired
    IDrinkResponsitory iDrinkResponsitory;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrink(){
        try {
            List<CDrink> list = new ArrayList<CDrink>();
            iDrinkResponsitory.findAll().forEach(list::add);
            return new ResponseEntity<List<CDrink>>(list, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.jparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.jparestapi.models.CDrink;

public interface IDrinkResponsitory extends JpaRepository<CDrink,Long> {
    
}

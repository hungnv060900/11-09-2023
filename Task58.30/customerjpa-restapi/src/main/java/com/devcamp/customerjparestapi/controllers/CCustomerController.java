package com.devcamp.customerjparestapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerjparestapi.models.CCustomer;
import com.devcamp.customerjparestapi.responsitory.ICustomerResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CCustomerController {
    @Autowired
    ICustomerResponsitory iCustomerResponsitory;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomer() {
        try {
            List<CCustomer> listC = new ArrayList<CCustomer>();
            iCustomerResponsitory.findAll().forEach(listC::add);
            return new ResponseEntity<>(listC, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

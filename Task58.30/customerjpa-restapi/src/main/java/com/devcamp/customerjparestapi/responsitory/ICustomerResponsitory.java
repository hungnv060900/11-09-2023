package com.devcamp.customerjparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerjparestapi.models.CCustomer;

public interface ICustomerResponsitory extends JpaRepository<CCustomer,Long> {
    
}

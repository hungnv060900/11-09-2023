package com.devcamp.customerjparestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerjpaRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerjpaRestapiApplication.class, args);
	}

}

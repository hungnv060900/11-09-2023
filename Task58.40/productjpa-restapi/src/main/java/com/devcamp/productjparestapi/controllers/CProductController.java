package com.devcamp.productjparestapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.productjparestapi.models.CProduct;
import com.devcamp.productjparestapi.responsitory.IProductResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProductController {
    @Autowired
    IProductResponsitory iProductResponsitory;

    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllProduct() {
        try {
            List<CProduct> list = new ArrayList<CProduct>();
            iProductResponsitory.findAll().forEach(list::add);
            return new ResponseEntity<List<CProduct>>(list, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.productjparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.productjparestapi.models.CProduct;

public interface IProductResponsitory extends JpaRepository<CProduct,Long> {
    
}

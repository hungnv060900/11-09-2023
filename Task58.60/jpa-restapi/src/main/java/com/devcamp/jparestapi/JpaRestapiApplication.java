package com.devcamp.jparestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaRestapiApplication.class, args);
	}

}

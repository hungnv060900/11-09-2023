package com.devcamp.employeejparestapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeejparestapi.models.CEmployee;
import com.devcamp.employeejparestapi.responsitory.IEmployeeResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CEmployeeController {
    @Autowired
    IEmployeeResponsitory iEmployeeResponsitory;

    @GetMapping("/employees")
    public ResponseEntity<List<CEmployee>> getAllEmployee(){
        try {
            List<CEmployee> list = new ArrayList<CEmployee>();
            iEmployeeResponsitory.findAll().forEach(list::add);
            return new ResponseEntity<List<CEmployee>>(list, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

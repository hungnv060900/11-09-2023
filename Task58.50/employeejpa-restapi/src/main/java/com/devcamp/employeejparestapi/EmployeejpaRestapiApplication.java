package com.devcamp.employeejparestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeejpaRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeejpaRestapiApplication.class, args);
	}

}

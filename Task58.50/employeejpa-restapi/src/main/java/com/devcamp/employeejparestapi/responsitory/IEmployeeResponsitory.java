package com.devcamp.employeejparestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.employeejparestapi.models.CEmployee;

public interface IEmployeeResponsitory extends JpaRepository<CEmployee,Long> {
    
}
